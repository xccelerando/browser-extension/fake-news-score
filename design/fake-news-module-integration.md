# Fake-news-module integration

The fake-news-module integrates into the overall [architecture of browser-extension](https://xccelerando.gitlab.io/browser-extension/docs/architecture/). Here is a more detailed description of the integration in terms of architecture of the module itself and data flows. 

## Functionality

Input: 
1. web content (web page, social media post, etc.), 
2. database / sources of known fake news;

Output: 
1. statements and sentences in the text tagged as probable fake news;
2. fake-news-score of each identifies sentence (0-10), 10 indicates certainty.
3. corresponding known facts from the database of known fake news;

The module itself should contain an algorithm, which aggregates the information about probability that certain sentence is a fake news and calculates the score i.e. number from 0 to 10. 

## Scenario

When the xdo-browser-extension is installed on user's browser, a web page viewed by a user is analyzed for fake news. Fake-news-score module (1) converts the web content into plain text (2) queries all available data sources for estimating the 'truth value' of the content, either sentence by sentence or as a whole (depending on the source); (3) weights the estimations provided from each source and outputs the final score between 0 and 10; (4) the browser extension UI displays the score to a user viewing the checked web content and provides a way to drill into the sources of the information.

## Sources

Sources of fake news can be divided into three categories:

1. **general public**, reading articles and providing feedback;
1. **fact checking experts**, including news agencies, associations, etc which specialize is fact checking (for more see [here](https://gitlab.com/xccelerando/browser-extension/data-ingestion-api/-/issues/1));
1. **AI / ML algorithms**, designed for detecting bias, hate speech, fake speech, etc (see [here](https://gitlab.com/xccelerando/browser-extension/fake-news-score/-/issues/4)).

Each category may contain more than one actual data source and module for accessing it. For example, knowledge from general public may be collected via feedback from browser-extension, reputation system; from fact checking experts -- from specialized RSS feeds, twitter accounts or website scraping, from AI/ML algorithms -- via running these algorithms on each web content.

## Data ingestion

Depending on the source, query API may call database of fake news, which have to be constructed in advance via data-ingestion-api, or directly AI/ML algorithms. While information from general public, which will most probably will be coming via browser extension (therefore we will be able to ingest it directly to the behaviours-database), the information from third party sources (i.e. fact checking websites, news agencies, etc.) will have to be ingested into a fact-news-database before it can be queried. The mechanism of ingestion can differ depending on the source (e.g. RSS feeds or tweets) but is called data-ingestion-api in general case.

## Schemas

### Integration architecture
<img src="schemas/fake-news-score-integration.png" width="800"> 

### Query API dataflows
<img src="schemas/query-api.png" width="800"> 

## Modular development

The overall architecture and its APIs will be developed by modules, therefore allowing different developers and community volunteers to contribute considering their knowledge and interests. Furthermore, the modular architecture allows for the functionality to evolve, by allowing new modules to be developed and integrated. In the future, the back-end architecture of the browser-extension will rest on SingularityNET AI services.

## See more

* [Project documentation](https://xccelerando.gitlab.io/browser-extension/docs/)
* [Project source repositories](https://gitlab.com/xccelerando/browser-extension)
* [SingularityNET AI services marketplace](https://beta.singularitynet.io/)
* [SingularityNET AI services publisher portal](https://blog.singularitynet.io/the-singularitynet-publisher-portal-open-beta-41eba3bee56e)
